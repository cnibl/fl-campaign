Rosenbrant
===========

En mindre utpost med 18 invånare. Byggdes ursprungligen före blodsdimman, för över tusen år sedan, men har legat övergiven under långa perioder. Nu styrs byn av en grym borgmästare. Ligger ovanför ett brant stup ner mot Laak Varda. En livsfarlig stig leder upp och ner till vattnet, där en liten hamn finns med ett par mindre båtar förtöjda.

Under många år har folk flyttat därifrån efter att orcherna börjat bygga upp Rosenöga till sin egen, och blivit mer och mer revirhävdande i området. För ett par år sedan dök dessutom ett slukhål upp mitt i byn. Det sägs att farliga varelser kommer ur sprickorna i botten av hålet, och detta blev spiken i kistan för Rosenbrant, som nästan övergavs helt. Nu står många hus i byn tomma, och det är konstant någon som vakar vid hålet.

Institutioner
-------------

**Magra vildsvinet**, ett litet men välkomnande gästhus, med ett rejält långbord. De tar för mycket betalt för en svampstuvning. Någon eller några på väg till eller från Rosenöga är ofta här. Ibland även orcher.

Personer
----------

**Osgar**, en feg och plikttrogen människa med vackra drag. En av Merigalls avkommor. Långt stiligt hår uppsatt på ett imponerande vis. Planerar att hämnas mordet på sin mor, som ska ha mördats av ett orchband från Rosenöga (men i själva verket dödades av Merigall för att hon hotade att avslöja honom).

**Merigall** besöker ofta byn på väg till eller från Rosenöga. Han träffar och samtalar då och då med orchdrottningen Soria om Ravland, Zytera och framtiden, för att försöka bedöma om hon är en lämplig person att leda Ravland när Zytera störtats (i första hand ett styre delat med andra).