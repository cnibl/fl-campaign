Flodkrok
==========

En knutpunkt. 208 invånare. Byggdes 150 år sedan, under dimmans tid. 

Omges av åkrar och skogsområden.

Utanför byn hänger döda rostbröder, uppspikade på pålar, som avskräckning.

Styrs av en grym kommendant, Sithric, som stolt talar om hur han hållit rostbröderna stångna de senaste åren. Skämtar hela tiden, ingen vågar annat än skratta. Har en mörk hemlighet. Närmaste tjänaren är Atheric

Byn plågas av räder som utförs av järngardistiska desertörer, som lämnat rostkyrkan och övergått till plundring.

Känd för dekadent leverne, förbereder ett bröllop mellan Sithrics son (Balderich) och en kvinna från Eners pik (Morion).

En del båtar går längs floden söderut mot Kumleklint och vidare mot Vargekrok. Med stridigheterna som pågår mellan rostkyrkan och Zertorme så är båtarna dock få.

Institutioner
--------------

3 handelsbodar: vargmannen *Chenofur*, korpsystern *Tuukka* (örter etc.), halvlängdsmannen *Godeliva*

Jägmästare: Hariwald. Dyster. Plågas av mardrömmar av sin familj som dör i lågor. Bor i ett skogshuggarläger utanför byn.

*Krukan & Hjulet*: slitet värdshus med stampat jordgolv. Generösa med rostad gris. En förklädd ädling finns bland besökarna. 

*Röda sparven*: trångt värdshus med fulla äventyrare som kan berätta många historier om Ravland. Generösa med dvärgiskt mjöd (drivs av en dvärg, stolt över sitt ursprung).

Personer 
----------

**Atheric av Kalkklippa** - This subservient Ailander is a Scribe. He is deformed and is exercising a pet, they smell terrible.
STRENGTH 1, AGILITY 2, WITS 4, EMPATHY 2
Skills: Lore 2, Insight 2
Kin Talent: Adaptive
Gear: Bound leather book, quill and parchment, 5 silver

**Sithric** - This irritable Ailander is the cruel commandant. He smells bad and is constantly joking, but hides a dark secret.
STRENGTH 3, AGILITY 3 ,WITS 2, EMPATHY 2
Skills: Melee 2, Scouting 1, Survival 2 
Kin Talent: Adaptive
Gear: Broadsword, leather armor, 3 copper

**Baldarich** - The son of Sithric. He has fluffy hair and is whistling, they twitch.

**Morion** - This brash Alderlander secretly worships Rust. She is graceful and is chewing on narcotic leaves, has a staring glare and a foul mood.

**Eadgar Orrick av Litwin Dreljus** - This miserable Dwarf runs the inn Röda sparven. She has round cheeks and is cast out from the dwarven clans for descending into the depths of Stormunsel . Constantly rub their eyes.
STRENGTH 2, AGILITY 2, WITS 3, EMPATHY 3
Skills: Endurance 1, Crafting 2
Kin Talent: True Grit
Gear: Tools, 1 copper


**Chenofur** - This stressed Wolfkin is an Actor. He is constantly smiling and is juggling, they chew beef jerky.
STRENGTH 2, AGILITY 3, WITS 3, EMPATHY 4
Skills: Lore 1, Insight 1, Manipulation 2, Performance 3
Kin Talent: Hunting Instincts
Gear: Prop Sword, a script, 5 silver, pouch with 11 silver coins


**Tuukka** - This agreeable Ailander is a Raven Sister. He has missing teeth and is possessed by a quirky demon.
STRENGTH 3, AGILITY 3, WITS 3, EMPATHY 3
Skills: Melee 2, Move 1, Marksmanship 2, Lore 3, Survival 1, Insight 2, Manipulation 1, Healing 2
Talents: Path of Healing 2, Path of Sight 2
Kin Talent: Adaptive
Gear: Staff, dagger, 6 copper

**Eohric** - This precise Ailander is a Child. He is heavily tanned and is seeking employment, they play with thier jewellery.
STRENGTH 1, AGILITY 2, WITS 2, EMPATHY 3
Skills: Performance 1, Sleight of Hand 1
Kin Talent: Adaptive
Gear: Tools, 5 copper

**Godeliva** - This naive Halfling is a Laborer. He is a hunchback and is squeamish and weepy. Owns a shop.
STRENGTH 2, AGILITY 3, WITS 3, EMPATHY 3
Skills: Endurance 1, Crafting 2
Talents: Builder
Kin Talent: Hard to Catch
Gear: Tools, 2 copper

**Melobren, son av Kaleerill** - This flighty Halfling is a Soldier. He has bloodshot eyes and is bankrupt, but has a grand plan to save the business. They just need some help.
STRENGTH 4, AGILITY 3, WITS 2, EMPATHY 2
Skills: Melee 2,Survival 1
Kin Talent: Hard to Catch
Gear: broadsword, long bow, large shield, studded leather armour, open helmet, 7 copper, Bronze Helmet worth 1 silver

**Hardmod av Skimmelskott** - This polite Elf is a Fisherman. He has bushy eyebrows and likes to touch people as if old friends. He regularly goes fishing in Laak Varda, avoiding the reptile men, but looking for the delicious catfish.
STRENGTH 1, AGILITY 2, WITS 3, EMPATHY 3
Skills: Crafting 2, Lore 2
Talents: Fisher
Kin Talent: Inner Peace
Gear: Knife

**Baldomar** - This thoughtless Dwarf is a Smith. He is dimpled and worships the god in the deep. Grumpy loner.
STRENGTH 3, AGILITY 3 ,WITS 2, EMPATHY 2
Skills: Melee 3, Scouting 1, Survival 2 
Kin Talent: True Grit
Gear: axe, 3 copper, Copper Crown worth 3 silver

**Köpmannen Morlind** – som varit i Rosenöga för att handla med orcherna
Motivation: Ett sätt att leda spelarna vidare till nästa Stanengistrubin.
Sinnesintryck: mager, svettig, hungrig
Vid bordet:

Kan berätta sägnen om Rosenöga. Reser med sin dotter Melene och två goda vänner, som också är knektar. Handlar med kläder och hattar. Har inget speciellt fint. 