Gargabrink
==========

En mindre by vid Gargaträsken, som byggdes 250 år sedan under blodsdimmans tid. 28 invånare. Byborna går klädda i eccentriska kläder, tillverkade av vass från träsket, och svartvingarna målar sig typiskt i ansiktet.

Instabil vapenvila med ödlemännen i träsket, som anser att marken i träsken är helig och tillhör endast ödlemän. Då och då uppstår mindre skärmytslingar men ingen sida vågar göra något mer av rädsla för att starta en storskalig konflikt.

Styrs av trollkvinnan Kuno, som leder en grupp svartvingar. Fanatiska eländer som anser det vara sin plikt att ta livet av alla som inte förstår sin plikt att lämna Ravland.

Kuno och svartvingarna vill snarast förbereda ett utrotningskrig mot ödlemännen i träsket.

En annan grupp är ormkyrkan. En traditionell trosinriktning som motsätter sig strid med ödlemännen, och är beredda att gå långt för att undvika blodsspillan.

Gargabrink är en gästfri plats. 

Handelsmän på väg till Rosenöga stannar ofta till i Gargabrink och övernattar.

Handelsplatsen
----------------

En handelsplats för handel med reptilmän finns någon timmes promenad norrut, vägen dit och platsen i sig är strikt fredad och inget våld förekommer så länge man håller sig på stigen.

Träbryggor leder ut till en träplattform som sluttar ned i vattnet, reptilmännen möter upp vid plattformen. Reptilerna byter till sig kött, metallföremål, fiskeredskap, spjutspetsar, ljuster mot fisk, purpursnäckor, grodor och brödrot.

Institutioner
----------------

**Dimmiga lyktan**, sömnigt värdshus som inte har så mycket aktivitet. Mysigt, med en glad hund. Tar dyra pengar för ugnsbakad mal. Serveras med stapelvaran brödrot. Föreståndaren är Hild. 


Personer 
-----------

**Kuno** - This depressed Ailander is a Blackwing. She has strange facial paint and sleeps badly, as they see holy visions at night.
STRENGTH 4, AGILITY 5, WITS 4, EMPATHY 3
Skills: Melee 4, Move 3, Stealth 4, Marksmanship 4, Lore 1, Survival 2, Insight 3, Manipulation 2
Talents: Path of the Killer 2, Executioner 2, Fast Footwork
Kin Talent: Adaptive
Gear: Broadsword or scimitar, dagger, light crossbow, vial of poison, studded leather armor, Rare Book worth 20 silver

**Earnwulf** - This garrulous Ailander is a Trader, trading with furs and leather. He is tired and is haggling, they play with their hair. On their way to Rosenöga. Needs to rent a boatsman and guards to travel through the swamp and across the lake.
STRENGTH 3, AGILITY 2, WITS 2, EMPATHY 2
Skills: Melee 2
Kin Talent: Adaptive
Gear: mace, leather armor,6 copper

**Wynstan** - This superior Ailander is a Blackwing. He is sweaty and has expensive habits, but will leave tab to others.
STRENGTH 4, AGILITY 5, WITS 4, EMPATHY 3
Skills: Melee 4, Move 3, Stealth 4, Marksmanship 4, Lore 1, Survival 2, Insight 3, Manipulation 2
Talents: Path of the Killer 2, Executioner 2, Fast Footwork
Kin Talent: Adaptive
Gear: Broadsword or scimitar, dagger, light crossbow, vial of poison, studded leather armor, Silver Bowl worth 12 silver

**Hild** - This sympathetic Ailander is an inn keep. He has unkempt eyebrows and worships the god in the deep.
STRENGTH 2, AGILITY 3, WITS 3, EMPATHY 4
Skills: Lore 1, Insight 1, Manipulation 2, Performance 3
Kin Talent: Adaptive
Gear: Scroll, dagger, 5 silver, pouch with 9 silver coins
