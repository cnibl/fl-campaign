Vargekrok
===============

- Stor by (~300 invånare). 
- Anlagd under blodsdimmans tid (ca 200 år sedan).
- Leds av en svag nasare vid namn *Brynder*. Har en handelsbod i ett gammalt torn i stadens mitt, och bor på övervåningen. Styr över en milis, som mest skyddar mot flygödlor.
- Känd för välsmakande vin, som odlas på kullarna runt om och slätterna ner mot sjön *Bludevann*. 

Inrättningar
---------------

- Värdshus, *Larmande Ulven*. Ägs av *Vide*. Specialitet är salt fågel och traktens goda vin. En stammis är den gamla krigsveteranen *Talinde*, som konstant ler. 
- Värdshus, *Pigan och Jungfrun*. Ägs av vresiga *Nirlinda*. Billig utspädd öl. En stammis är gamle krigsveteranen *Rode*, som luktar gott och är besatt av en demon från *Purpurtand*. 
- Smed, *Torvil*. 
- Mjölnare, *Harde*.
- Handelsbod, *Brynder*.
- Vinmakare, *Orovil*.
- Rostkyrka. En liten stuga samt en enkel helgedom. I stugan jobbar några blodssystrar och rostbröder. De kan hjälpa till med helning och läkande förband men kräver att man tackar Rost och Heme för deras hjälp. 

Personer
-----------

Talinde 
   Krigsveteran. Slogs för rostbrödernas räkning uppe i nordöst mot aslener och eländer, med *Rode*. Ler konstant. Är bankrutt men har en idé att tjäna stora pengar genom att plundra den gamle kung *Algarods* obevakade (?) krigskassa vid borgen *Vädersten*. Har träffat en av *Merigalls* avkommor med gula ögon, en handelsman som var på jakt efter *Gallöga*, en vacker ädelsten. Har svurit att lämna ädelstenen till handelsmannen om hon hittar den.

Rode
   Krigsveteran. Slogs för rostbrödernas räkning uppe i nordöst mot aslener och eländer, med *Talinde*. Luktar gott. Besatt av en nyckfull demon som håller till i *Purpurtand*. Vill locka personer dit för att besättas av demonen, som då kommer närmre att kunna anta fysisk form. Bor i ett ruckel i byns utkant, på gatan där de fattiga svartalferna i byn bor. 

Nirlinda
   Vresig värdshusvärd på *Pigan och Jungfrun*.

Vide
   Gemytlig värdshusvärd på *Larmande ulven*. 
   
Birema
   Garvare. Lite sur. Klädd helt i läder. Rädd för rostbröderna.
   
Oroden
   Kringvandrande köpman som är en av Merigalls avkommor. Handlar med ädelstenar och liknande varor. Håller oftast till kring Vargekrok. Söker stenen Gallöga för Merigalls räkning. Känner Talinde.
