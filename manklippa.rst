Månklippa
=========

Ett stort fäste, byggt av dvärgar 959 år sedan av en dvärgisk furste för att skydda den dåvarande huvudsakliga ingången i riket under bergen i Arinaskogen och gruvorna i området. Under åren försvann fler och fler dvärgar från fästet och när det anfölls av en här av orcher, blev resultatet en blodig belägring som orcherna gick segrande ur. Fästet övergavs då för gott av dvärgarna. Senare blev dessutom Lumra en viktigare gruva. 

Fästet är svart av sot, och förfaller mer och mer varje dag. Det består av flera väl befästa byggnader som skyddas av tjocka murar och en vallgrav med vindbrygga. I princip ointagbart vid full styrka. 

Idag står fästet oftast övergivet. Just nu bor dock en stam orcher där. Totalt 30 stycken krigiska orcher ur XXklanen. De tillfångatar personer ur andra släkten och njuter av att tortera och lemlästa dem, speciellt alver och dvärgar. 
