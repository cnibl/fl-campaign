Stanengist
==========

Kronan med alvrubiner, smidd av dvärgar för länge, länge sedan. Består av en krona med infattade alvrubiner. 

Rubinerna
---------

Namn. Var finns den? Vem vill ha den?
- **Gemelda**. Sitter i Stanengist.
- **Nejd**. Sitter i Stanengist.
- **Nebulos**. Sitter i Stanengist. 
- **Iridne**. I mantelspännet Hemella/Blodestjärna, i Rosenöga.
- **Viridia/Gallöga**. Sitter i svärdet Maligarn, i Lumragruvorna. Merigall söker den.
- **Algared**. Sitter i spiran Nekhaka, i Stormpalatset. 
- **Tusenhjärta**. Splittrad i skärvor, sitter i pannan på druider (Jungrysyskonen).  



Artifakter och effekter
-----------------------

**Stanengist**. 
   Effekt beroende på rubiner i den.
	
*Gemelda+Nejd+Nebulos*: 
   Antimagi utlöses automatiskt då besvärjelse kastas inom nära avstånd, effektgrad T6.
	
*Gallöga*: 
   Samma effekter som svärdet Maligarn utom vapenskada och -bonus.

*Algared*: 
   Samma effekter som spiran Nekhaka, utom tryckvåg.

*Iridne*: 
   Samma effekter som Blodestjärna, utom sväva.

*Demoniskt vansinne*: 
   Skadar demoner och demonsmittade, (antal stenar - 3)T6 på INT.

*Protonexus*: 
   Kronan stänger protonexus om den slängs in i det (kräver minst 4 stenar). 
	
*Montera nya stenar*: 
   Kräver lyckat slag för Hantverk.


