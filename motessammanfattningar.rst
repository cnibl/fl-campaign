Mötessammanfattningar
======================

Möte 20, 15/6
--------------
- RP undersöker trädgården. Träffar Mergolene, som ber dem byta ut rubinen och berättar om Kalman Rodenfell och alverna och rödelöparna, samt om att protonexus i Vond behöver stängas för att inte släppa in fler demoner.
- RP undersöker helgedomen och tar sig igenom hemliga gångarna upp till borgen.
- Akka förvandlar sig till korp och lyckas ta sig in och stjäla rubinen osedd (någon av viragerna eller kejsarinnan ser en korp flyga iväg med stenen).
- Avslutar dag 11, eftermiddag.


Möte 19, 3/6
-------------
- Pratar med Maligarn. Han vill ha Gallöga och svärdet Maligarn. Berättar att stenen är alven Viridias hjärta och att han var Viridias partner en gång i tiden men att svärdet stals från honom.
- Säger att han kan erbjuda kontakter med mäktiga personer, till exempel Zytera och drottning Soria. 
- De stämmer träff två veckor senare i Flodkrok. 
- Merigalls avkomma Osgar rekryteras till borgen och börjar vandra ditåt, under dag 11. 
- RP fortsätter mot Rosenöga. Framme eftermiddag dag 10. Bor på värdshuset Hrokas järnnäve. Träffar värdshusvärden Elma (erbjuder hjälp att komma in i borgen mot betalning), svartalfen Wulfric (bakfull), Jorma (tiggande eländer, tror han är kunglig). 
- Dag 11 fm. Lyckas ta sig in på audiens med Hroka med hattmutor. Talar med Hroka, som får höra att de kommer från riket "Ostifra" och "Ompalass". Hranga Gala är skeptisk. Kejsaren ska sova så RP behöver gå. De bjuds in till gästabud samma väll, som ambassadörer.

Möte 18
-------
- Tillbaka till Stormpalatset. Lämnar timret. Skogshuggarna har producerat 30 enheter trä (75% av fullt pga måste gå till/från skogen, 6 trä/dag 2 pers). 
- Bygger en åsnehage. Betalar för 10 ytterligare dagar (totalt 13 dagar från avfärd nästa dag)
- Beger sig norrut mot Gargabrink och Rosenöga. Stöter på en ensam krigshäst, som tillhörde furstesonen Lavide (ätten bor vid borgen vid östra hörnet av Laak Varda), Grok känner igen sigillet från en person som besökte värdshuset i Flodkrok.
- Anländer Gargabrink. Talar med handelsmannen Aernwulf, som handlar med kläder och pälsar. Beslutar att eskortera honom till Rosenöga, för 2 silver per dag plus 2 extra när de kommer fram (inte betalat).
- Tar sig mot Rosenöga landvägen. Anländer Rosenbrant, där de träffar Osgar och får höra om slukhålet och plundrande orcher.
- På det nästan övergivna värdshuset kommer Merigall in genom dörren. "Jag har förstått att ni söker vissa alvrubiner..."

  
Möte 17
----------
- Nirmena beger sig av. De beger sig till Flodkrok.
- Stöter på vilsna aslener i dimma som är på väg till Eners pik. De berättare om Zertorme och Krasylla. 
- Hittar en död vargman som bär på en karta till Rosenöga och någon form av karta över en byggnad eller något annat (alvtemplet).
- Pratar med alven Hardmod och vargmannen Chenofur i Flodkrok, får höra om reptilmännen i Gargaträsken.
- Pratar med köpmannen Morlind, som varit i Rosenöga och berättar om orcherna där och att kejsar Hroka den förste och störste sägs ha fått tag i en alvrubin.
- De köper 50 enheter trä i skogshuggarlägret och börjar vända tillbaka mot Stormpalatset.

Möte 16
--------------
- Tränar och övar färdigheter i lägret.
- Spanar på borgen och ser hur bandet ger sig ut utom utkiken i tornet. Beger sig dit, överrumplar utkiken och tar över borgen efter att ha vunnit över alla utom Nirmena på sin sida, och utsatt Nirmena för skräckattack.
- Får höra om hur rövarnas tidigare ledare Varg Vindsnabbe och hans magiska svärd. Han begav sig av med en grupp mot Lumragruvorna för att leta dvärgskatter och har inte hörts av sedan dess.
- Anlitat övriga rövare som tjänare.

Möte 15
--------------
- Flyr från blodlingen. Fortsätter in.
- Stöter på takfällan. Animerar ett skelett för att sticka in handen i gapet och öppna luckan. 
- Smyger in till sarkofagen, besegrar dödsgasten och hittar den riktiga skatten bakom hemliga dörren. 
- Tillbaka till lägret.

Möte 14
--------------
- Tar sig fram till Stormpalatset. Har sett på avstånd att det lyst där om natten och tar sig försiktigt fram. 
- Ser en vaktpost men håller sig dolda. Smyger fram på kvällen och rundar borgen. Hör ljud av rövarna som håller till i borgen.
- Tar sig in genom att bryta upp ett galler nere mot sjön och kommer in i en vattenfylld passage som leder till köket i källarplan. Viker av mot besvärjarens grav.
- Går in i graven. Ser väggmålningarna på besvärjaren med Nekhaka och Stanengist. Fortsätter fram mellan fällor, hittar en skatt men överrumplas av en blodling [ska precis slå initiativ].

Möte 13 
--------------
- Fortsätter norrut mot Stormpalatset. Tränar fladdermöss.
- Överrumplas av blodsdimma och en blodling, som åsamkar stor skada innan de flyr. Drabbas av katatoni och demonsmitta. 
- Fortsätter norrut. Får reda på Stanengists effekt. Inget mer händer.

Möte 12
--------------
- Hänger i Vargekrok ett tag. Undersöker smedens son och andra ungdomen som tagits av Rode i demonform. Hittar platsen där de hängt bland vinrankor utanför byn. 
- Undersöker Rodes hus, hittar underliga spår. Hornmärken i väggen, inslaget tak etc (Rodes kropp har övertagits av demonen i Purpurtand, har tagit ungdomarna dit).
- De lämnade byn och gick norrut. Träffar kultister som söker Kunskapens spira, som de säger ska finnas i Stormpalatset, uppe vid Drejarens öga. Blir höga med kultisterna och lurar dem att spiran finns söderut i Vädersten och beger sig norrut.


Möte 11, 15/1
--------------
- Slår läger (undviker minotauren) och vilar upp sig. Går in i N45, undviker ett möte med rövare (möte 12). 
- Fortsätter genom redan utforskade hexar mot Vargekrok. Lägger bakhåll för tidigare upptäckta rövare (möte 12) men låter de passera utan att ge sig till känna.
- I Vargekrok får de höra att smedens son och mjölnarens dotter har försvunnit senaste veckan.
- Får legenderna om Haggahus och Lumragruvorna. OBS: har gett fel plats för Haggahus. Behöver korrigera. Eller tänka att personer sagt fel.
- Träffaren svarvaren Gorganna, en svartalf. Svarvar ljusstakar.
- Akka börjar tämja en svärm av fem fladdermöss. Behövs ytterligare 4 lyckade slag över 4 dagar.


Möte 10, 16/12
---------------
- Undersöker vakthuset. Fortsätter snart till Algarods torn. Undersöker en gästabudssal men fortsätter sedan uppåt till toppen, där de först går igenom vaktrummet innan de kommer till Algarods rum, där en smal trappa leder upp till en terrass med ett gammalt rostigt teleskop.
- Porträttet skriker och de rusar fram och börjar bränna upp det.
- Kaewe greppar svärdet, och de döda vaknar. Sliter åt sig det och hamnar i handgemäng med kungen. Bakom vaknar livvakterna.
- Tuff strid bryter ut där Kaewes fot och Talindes ben kapas av de mäktiga skelettvakterna. Utanför hörs en underlig gammalmodig sång i vinden med ett mäktigt skri som besvarar det. 
- Efter att ha besegrat vakterna talar de med den döde Algarod och får svar på många frågor om Therania, Zygofer och annat. Bland annat säger kungen att Therania bor i Alderkleva. 
- De flyr med hjälp av gripen men ser hur en hord av odöda vandrar blint ut från borgen, bort i dalen. Lämnar liket av den nu döde gripen bakom sig och skyndar in i nästa hex, norrut över bergen.
- Hör ett sorgset bestialiskt vrål bakom krönet, där står en minotaur vid en död artfrände och ett gäng döda knektar. En skogsdunge syns en bit bort.
- Avslutar bakom krönet. Minotauren börjar ge sig av för att hämnas. De måste lyckas smyga för att komma undan den.
- OBS: Fredrik ska slå för pilar (minst en gång). Regler: orchtalang aktiveras start på ens nästa tur, man får en kritisk skada. 
	
Utforskade hexar:
	O47
	


Möte 9, 4/12
-------------

- Börjar undersöka vägen in i borgen, vindbryggan har ruttnat sönder. När Akka undersöker en blodfläck vid det leriga vattnet försöker en tentakel dra ner honom i vattnet men misslyckas. Det blir sedan tyst.
- Flyger över med gripens hjälp och undersöker riddarhuset. Hittar inget speciellt och fortsätter mot ravinen.
- Harpyorna anfaller, men gripen slår tillbaka. Efter att de dödat flera av harpyorna flyr dessa tillbaka. 
- RP fortsätter in i labbet. Där anfaller skorpionmonstret nästan direkt men med stor hjälp av magin björntass har de ganska snabbt ihjäl den. I rummet finns rostbrodern Feribald som gömt sig i ett hörn. Han är skadad (avhuggen hälsena) efter strid med monstret och mycket tacksam när de hjälper honom, och gripen tar honom ner till barden Dalb. [Han kommer föra ryktet om Akkas björntass vidare.] 
- RP fortsätter till teatern där harpyorna håller till. Strid utbryter mellan dessa och gripen medan RP är på andra sidan dörren. Snart börjar de dock förhandla. Harpyorna ger RP halva sin skatt men vill ha ett spädbarn i utbyte.
- Akka går tillbaka till labbet och hittar ett hundfoster som Isrot i en mörk ritual väcker upp från de döda. De lyckas lura harpyorna att detta är ett spädbarn och har nu vapenvila samt får halva skatten (4 g, 7 s, 11 k). Om ett kvartsdygn när fostret återvänder till de döda kommer de bli mycket upprörda..
- RP går upp på terrassen, ser katapulten och fortsätter mot vakttornet uppe vid Algarods torn. Gripen flyger dem över ravinen, avslutar med att de blickar in i det stora vaktrummet.
	
	

Möte 8, 19/11
-------------

- Flyr från Dimhus patruller mot Vädersten.
- In i hex N47. Inget där utom ingången uppe på berget [gammal kulthelgedom]. Lägger ett kvartsdygn på att bygga en bår att transportera gripen på. Fortfarande förföljda av en patrull och flyr i sista stund.
- In i hex O48, stöter på resen Kurge som bär på en säck med en rostbroder i [möte 24]. Övertalar resen att ge sig på den jagande patrullen och fortsätter vidare.
- In i hex P47 [Vädersten]. Hittar Rolk [från Esgar Färdings gäng] och förhör honom hårt (han vill mest ha mer vin). Isrot ska ut och söka föda men går vilse i dimma och kyls ned. Följer en mystisk hjort och hamnar vid borgen och vakttornet där det lyser. 
- RP övernattar i ett eget läger och övervakas Esgars läger. I gryningen kommer Esgar flygande genom luften från borgen och krossas mot marken [skjuten av harpyorna med katapulten].
- Fortsätter mot borgen, pratar med Dalb, som berättar många historier från Ravland [verkade lite misstänksam enligt spelarna] och verkar extra intresserad av rubinen Gallöga.
- Avslutar utanför borgen vid vallgraven.

Utforskade hexar:
   N47, O48, P47

Övrigt:
	Rör sig 3 hex/2 kvartsdygn med båren.


Möte 7, 5/11
-------------

- Slår läger vid den döda gripen. Madwir och Kaewe jagar (hjort/sork). Isrot väcker upp gripen från de döda.
- Isrot flyger iväg på gripen medan de andra vandrar söderut. Isrot upptäcker ett litet sällskap med en åsna och vagn en bit söderut. Ser en liten by en bit bort söderut. Kan se grottingångar i bergen ovanför passen, den västra ser dvärgisk ut. 
- Anfaller sällskapet med vagnen, låter gripen döda människorna och jagar efter åsnan. Åsnan flyr för sitt liv. Vagnen fylld med mjölsäckar. Inte några direkta värdesaker, ett bronshorn.
- Isrot flyger sedan vidare och ser vad som verkar vara Vädersten längre österut. Jagar sedan rätt på åsnan och vagnen och slår läger utanför byn. 
- Övriga vandrar söderut och når byn Dimhus. Värdshus på torget, en liten handelsbod med puckelryggig försäljare och ett gammalt tempel som verkar funnits längre än resten av byn, som ser ut att ha byggts upp bara de senaste åren. Folk verkar berusade och skeptiska. Värdshusvärden indikerar att något är skumt. En viss Rynder observerar RP i värdshuset. Utanför byn finns små övervuxna jordhögar med hål bredvid här och där. 
- Akka sover utanför byn med Isrot och börjar undersöka hålen. Hittar spår av något insektsliknande skal, fast stort. Gången fortsätter mot byn. Hör klapprande ljud, som av massor med hårda ben som stegar fram i gången. Flyr. 
- Madwir och Kaewe vaknar i gryningen av att någon är utanför och pratar om att "hon kräver att få dem" och någon, som kan vara Rynder, menar att han sett henne i visioner om natten. Madwir och Kaewe flyr genom bakdörren och rusar ut ur byn mot de andra. 
- Avslutar med att de börjar fly från byn i gryningen, och ser hur flera patruller med insekter och människor ger sig av ut från byn.

Utforskade hexar:
   M46, M48 [Dimhus]

Möte 6, 23/10
-------------
- De går genast till attack mot de rödklädda sektmedlemmarna. Lyckas effektivt nedgöra allihopa utan större problem, och övriga i följet längre bort flyr då [de återvänder lite senare när RP lämnat för att begrava sina vänner]. Snor med sig några vapen och fortsätter söderut.
- Fastnar i kuperad terräng när de kommer in i Hargapasset. Där har en grip sitt revir, som upptäcker dem och försöker mota bort dem med hotfulla skrik och utfall.
- När gripen kommer närmre flyr de in i en klippskreva och lyckas med en fälla få gripen att kedjas fast vid en stenbumling. De lyckas nedgöra gripen men Kaewe faller och blir av med ett finger när gripen hugger med sina klor.

Utforskade hexar:
   N41, N43, M44 [missöde] 
   [1 hex kvar detta kvartsdygn]

Möte 5, 25/9
------------

- Kaewe försöker stjäla vapen från vakter som dödats i striden mot ödlorna, vilket inte är populärt eftersom dessa borde tillfalla offrens familjer.
- Isrot försöker ta hand om huden från ett av ödleliken och tar hjälp av garvaren Birema som garvar skinnet och gör en stark läderrustning av det. 
- Kaewe får förband vid rostkyrkan men vägrar tacka Rost och Heme efteråt och får inte komma tillbaka igen. 
- Efter ett par dagar beger de sig av söderut mot Vädersten med Talinde. Rode syns inte till och kommer inte med. De stöter på köpmannen Oroden utanför byn, som känner Talinde. De nämner stenen Gallöga och Talinde lovar att föra stenen till Oroden om hon hittar den i Vädersten. 
- Lite senare under dagen ser de en grupp orcher, som de bara gömmer sig för utan att närma sig ytterligare. Därefter kommer ett underligt följe med personer som verkar späka sig själva på olika sätt. Gömmer sig bakom en sten men förförs av någon sorts magi hos ledaren och upptäcks. Avbryter i detta ögonblick, när några personer befinner sig i närheten av stenen på väg dit.

Utforskade hexar:
   Q38, P39, O40 
   [1 hex kvar detta kvartsdygn]

Möte 4, 11/9
---------------

- Pratar med en vakt, Brand. Hör att Brynder bor i tornet på torget och har en handelsbod. 
- Alven Korpikalla sover utanför byn och sover i ett träd, där han nästan hittas av de förklädda rövarna från tidigare. Dessa rövare är nu alltså precis utanför (eller kanske i) byn.
- Orchen smyger in i Rodes hus i svartalfernas gränd. Blir påkommen av Talinde, som dock inte verkar bli så upprörd och istället undrar om äventyrarna är bra på att slåss och sugna på äventyr.
- Talinde erättar över en middag om den obevakade krigskassan i Vädersten, och erbjuder dem att följa med och dela på vinsten. Hon tror att det ligger i princip rakt söderut.
- Rode bjuds in men påstår att den bästa vägen går förbi Purpurtand. 
- Flygödlor anfaller byn i gryningen. Rollpersonerna utom Kaewe barrikaderar sig inne på värdshuset där de sovit. Kaewe beger sig ut och slåss. Efter en stunds strid har ödlorna fallit eller gett sig av, men många bybor ligger döda i byn och flera vakter uppe i tornet har dött i attacken.

Utforskade hexar:
   Ingen
   
Möte 3, 28/8
---------------

Ni insåg att rostbrodern i Kumleklint börjat utföra förhör med alla i byn för att reda ut vem som bränt båtarna och tyckte det var ett lämpligt tillfälle att dra från byn söderut. 

Ni begav er ut i vildmarken. Stötte på en blodig massaker som visade sig vara ett gäng förklädda rövare som dock skrämdes bort med magins hjälp. Ni tog er vidare i vildmarken och slog läger för att gå vidare nästa dag till en by som verkade finnas i närheten, men efter lite dåligt vildsvinskött drabbades ni av matförgiftning och var tvungna att stanna i lägret. Dagen efter detta drabbades ni istället av ett kraftigt hällregn och kom ingenstans den dagen heller. 

Nästa dag kom ni till slut fram till byn Vargekrok i änden av sjön Bludevann. Det visade sig vara en ganska stor by med några hundra invånare som led av attacker från blodtörstiga flygödlor som håller till i bergen i närheten. Ni kommer in på värdshuset Pigan och Jungfrun och samtalar lite med den vresiga värdshusvärden där.  

Borgen Purpurtand ligger några timmars vandring bort. Den säga vara hemsökt. Där bodde för längesen fursten Drukus som gick under i hungersnöd. Den ärrade krigaren Rode tycker dock det bara är skrönor att den skulle vara hemsökt och pratar också om att furstens skatt kan finnas kvar i borgen. 

Utforskade hexar:
   S34, R35, Q36 (Vargekrok)

Möte 2, 23/8
---------------

- Svartalferna bränner Ywims båtar och skär loss förtöjningen.
- I kaoset nästa dag anklagar Ywim Pollmor för det inträffade och ber Kaewe och orchen att mörda henne. De kan få en silverhjälm som belöning (värde 8T6 sm). 
- Pratar med korpsystern och får höra sägnen om Vädersten och om Nekhaka. - Utpressar henne att ge dem helande salva.
- Går upp till stencirkeln vid midnatt men där är gastar och skelett. Keawe blir påkommen av gasten och livet sugs ur honom med iskyla men han överlever till slut. De lyckas se att månen skiner på ett gravvalv nere på gamla kyrkogården.
- Väcker en odöd, Rygos, som var krigare i furst Nepolas armé under Zygofer. 
- Berättar om sin härförare Zygofer och hur de försöker driva bort alderlänningarna.
- Beger sig till gravvalvet och tar sig in. Där är furstinnan Ursula som ondgör sig över besvärjaren Zygofer och ber rollpersonerna hämnas hennes familj. Hon ber dem ta kål på Zygofer som borde finnas i staden Alderkleva (hon vet inte att det är en ruin) men är en mäktig besvärjare. Ingen öppnar furst Nepolas sarkofag och inget avslöjas om Ursulas förräderi. 

Utforskade hexar:
   Ingen

Möte 1
---------

- Börjar i skogen, på väg till byn Kumleklint.
- Kommer till Kumleklint, träffar Ywim som är otrevlig, går vidare till Död mans hand och hör om Fru Pollmor och konflikten. 
- Träffar ett antal SLPer, så som korpsystern, rostbrodern.
- Är uppe vid stencirkeln och ser inskriptionen.
- Får i uppdrag av Pollmor att bränna Ywims båtar.

Utforskade hexar:
   T33 (Kumleklint)


