Mötestabeller
=================


Harga
---------
1. 1T6 odöda från Vädersten
2. 2T6 odöda från Vädersten
3. 2T6 rostbröder, vallar en grupp av 2T6 odöda med kraftspröt. Leds av den demonvandlade järngardisten Rimmer, har ett extra huvud på ryggen täckt av en kåpa, som han samtalar med nattetid.
4. Ett kompani soldater, på väg hem från korståg i öst, ledda av järnriddaren Martas (giftiga huggtänder, vapenskada 1, dödande gift giftstyrka 6).
5. Sällskap på väg till Haggahus, med två ynglingar med som ska offras (dessa binds nattetid). Leds av rostbrodern Sallius som ser till att bönderna håller tyst vid behov. 
6. 2T6 kultister från Alderkleva. På jakt efter Kunskapens spira, för att leverera den till sin ledare Mastra. Spiran sägs stärka bärarens inflytande över andra människor, så länge bäraren är rätt person. Ska finnas vid furstinnan Manuxias grav.
7. 2T6 äserorcher, har 1T6 fångar i en träbur som ska offras till spindelgudens ära djupt inne i Hemeskogen.
8. Arvia Stordotter av Kromb, berättar om stulna fästet Vond, om Zytera. 
9. Virelda av Blodnäbb, söker Teramalda
10. 2T6 slavhandlare från Ramakved, snabba och sugna på att fånga fler slavar när ingen ser.
11. Kartorda med följe
12. Zytera med följe
13. Merigall, förklädd

Arinaskogen
---------------

Hemeskogen
--------------

Moldena
------------
1. Köpmannen Morlind, som varit i Rosenöga för att handla med orcherna
Motivation: Ett sätt att leda spelarna vidare till nästa Stanengistrubin.
Sinnesintryck: mager, svettig, hungrig
Vid bordet: torka av pannan, 

Kan berätta sägnen om Rosenöga. Reser med sin dotter Melene och två goda vänner, som också är knektar. Handlar med kläder och hattar. Har inget speciellt fint. 

2.  Aslener på väg till Eners pik
Motivation: Ett sätt att introducera Eners pik och Zertorme.
Sinnesintryck: Luktar häst och hö, underlig brytning, ovanliga kläder
Vid bordet: underlig brytning (spansk)

Kvarder, övertygade om att Zertorme är guden Horns utvalde frälsare som ska döda Zytera och återerövra Aslene

Markinus Garvare, Lokia, Fulcinia, Telesina, Naso, Tertius

3. Reptilmän. Fredlösa, utkastade från Drak yul i Gargaträsken. Rövar och plågar alla de kan. 
Motivation: Ett sätt att introducera reptilmän och Drak yul. 

4. Reptilmän. En grupp, utsända från Drak yul för att finna det mäktiga spjutet Egelte (Ivelde, s. 135). Avvaktande men inte våldsamma om det kan undvikas.

5. Zertorme med en här av aslener och eländer. På väg mot Eners pik efter ett slag mot rostbröder. Bjuder in rollpersonerna till sitt tält efter att ha fått höra att de finns i lägret.

6. Virelda Blodnäbb. Kan berätta sägner om rostfursten Kartorda och Zytera.

7. Ett slagfält. Någon skriker av smärta, bortom räddning. Rostbröder och järngardister har kämpat mot Zertormes här.

8. Band av järngardistiska desertörer. Opportunister. Gör allt för pengar. Brutala och utan gränser. 

9. Band av järngardister, på väg till Eners pik för ett hemligt uppdrag – att mörda Zertorme. Skickliga krigare. Vänliga mot de som visar vördnad för Rost och Heme.


Margelda & Sickla 
---------------------

Zertorme
Aslener, galdaner
Aslener, kverder (?)
Arvia Stordotter av Kromb

Vidanland
-------------

Resar
