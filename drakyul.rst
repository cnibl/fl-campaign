Drak yul
============

Reptilmanssamhället i Gargaträsken.

50 invånare. Grågrön fjällig hud, gula ögon, vassa tänder. Stinker av ruttet kött (de låter kött ruttna under vattnet för att lättare kunna slita det i bitar, som de måste sluka hela).

Omgivet av dammar, liknande bäverdammar, som dämmer upp vattenflöden för att skapa större undervattensrevir för reptilerna. Skyltar med text "inkrektare kekas", "fremling du kött". Mycket revirhävdande.

Reptilmännen lever på små holmar ute i de dimmiga träsken, i bäverdammsliknande hyddor byggda av vass. 

Dresserar krokodiler, som de använder som dragdjur.

Reptilmännen dyrkar sällsynta jätteålar som ibland kan skymtas från träskpråmar. Blågrönskimrande varelser, som reptilmännen tror är reinkarnerade mäktiga krigare.