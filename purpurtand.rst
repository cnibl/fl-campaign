Purpurtand
===========

- Stor borg, täckt av purpurblomster, som syns på långt avstånd.
- Byggd under blodsdimmans tid, ca 150 år sedan. Drog nytta av byn *Vargekrok* i närheten.
- Byggdes för att vakta en viktig handelsled som gick förbi *Vargekrok* och säkra vinhandeln.
- Byggdes av en grym furste vid namn *Drukus*.
- Nu helt ödelagd och obebodd.
- Invånarna gick under i hungersnöd.
- Förbannad (6T skräckattack varje kvartstimme). En demon, *Verganix*, håller till i borgen.


Demonen Verganix
------------------

En demon utan form är bunden till fästet. Livsessensen finns i en amfora i ett rum i källaren. Fursten *Drukus* var ursprungligen rostbroder men började efter en tid att dyrka demonen och kallade den till sig. Demonen tog furstens livskraft för att kunna ta form i världen och förbannade hela området, som drabbades av missväxt. En alv, *Livavår*, betvingade demonen delvis och tvingade livsessensen ner i en amfora vilket band demonen till borgen.

Skräckattack
   Demonen drabbar alla som är i borgen med skräckattack (6T/kvartstimme). Om man blir bruten kan man bli besatt av demonen. 

Mål
   Demonens mål är att anta fysisk form igen och terrorisera omgivningen. Detta uppnås med blodsoffer, som förs till borgen av de som blivit besatta av demonen. Om inget görs kommer detta ske om ca en vecka när *Rode* lyckas få med sig några intet ont anande lycksökade till borgen (rövarbandet som lurades med massaker?). 
