Rollpersoner
===============

Isrot / Grok (P)
------------------

*Orch, magiker*

Stolthet 
   Har sett döden i vitögat. Spelat schack med döden och överlevt.    

Mörk hemlighet 
   Nedfryst f.d. demontjänare, var på Zygofers sida.


Keawe / Vindhugg (S)
---------------------

*Vargman, krigare*

Stolthet
   Backar aldrig för en strid.   

Mörk hemlighet
   Njuter i hemlighet av att skada och döda andra. 


Madwir ormtjusaren (F)
-----------------------

*Halvalv, jägare*

Stolthet
   Din pil träffar alltid sitt mål.    

Mörk hemlighet
   Du lämnade en gång en skadad vän att dö i skogen för att rädda dig själv.


Akka Korpikalla Vallamo / Barkpäls (D)
---------------------------------------

*Alv, druid*

Stolthet
   Du anar onaturliga förändringar före alla andra.    

Mörk hemlighet
   Du har svårt med sociala sammanhand och måste isolera dig i skogen under längre perioder.