Dimhus
=======

By. 65 invånare. Byggd för ca 5 år sedan, när blodsdimman försvann. 

Styrs av ett brutalt råd och lider av vitt spritt fylleri. Rådet leds av **Rynder**.

Allierade med en stam insektoider, som lever i gångar under byn.

Platsen är känd för en horribel massaker. Byn består mest av skjul som byggdes upp en bit ifrån platsen där rostbröder brutalt mördade stora delar av invånarna. 

I mitten av byn finns ett gammalt tempel. Idag dedikerat till Insektsguden. Under templet finns en gång till insektoidernas drottning, som lever i en mäktig sal under jorden.

*Värdshus*: **Klagande kruset**, slitet med en sjungande värd. Serverar stuvade rovor. Irriterande narr som alltid är på plats.

*Handelsbod*: **Bundes bod**.