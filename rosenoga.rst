Rosenöga
=========


**Soria**: Har talat med Merigall, som vet att hon har Iridne och berättat för henne om Stanengist och protonexus i Vond. Soria vill få tag i Stanengist, och med en orchhär ledd av hennes make marschera mot Vond.

**Rödelöparna**: Leds av Kalman Rodenfell, som fått nys om att Iridne finns i borgen. 

Byn nedanför borgen
--------------------

Här bor ett hundratal orcher. Personer från andra släkten bor på Hrokas järnnäve.

**Hrokas järnnäve**: Ett enkelt värdshus, som drivs av aslenen Irma, som är en illaluktande lycksökare som hoppas tjäna stora rikedomar på orchernas dumhet. Kan utnyttja sina kontakter bland orchvakterna för att få in någon i borgen som handelsman, och i vissa fall till och med inför kejsare Hroka själv, men tar betalt för tjänsten. Ett tiotal personer från andra släkten, framförallt handelsmän bor här tillfälligt.

En igenvuxen gammal alvisk trädgård ligger i norra kanten av byn, längs med klippan upp mot borgen. Blommor hänger fortfarande bland vackra träd som snirklar sig upp mot himlen. I vissa fall ser det ut som att träden har ansikten.